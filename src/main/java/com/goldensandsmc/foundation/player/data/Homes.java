package com.goldensandsmc.foundation.player.data;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.NoSuchHomeException;
import com.goldensandsmc.core.player.PlayerDataContainer;
import com.goldensandsmc.core.util.LocationWrapper;
import com.goldensandsmc.core.util.Utils;
import com.google.common.collect.ImmutableMap;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class Homes extends PlayerDataContainer {
	private final HashMap<String, Integer> homes = new HashMap<>();

	public Homes(UUID player_uuid) {
		super(player_uuid);
	}

	@Override
	public void load(Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT `name`, `location_id` FROM `homes` WHERE `player_uuid`=UNHEX(?)")) {
			statement.setString(1, Utils.stringFromUuid(getPlayerUuid()));
			try (ResultSet result = statement.executeQuery()) {
				HashMap<String, Integer> temp_homes = new HashMap<>();
				while (result.next()) {
					temp_homes.put(result.getString(1), result.getInt(2));
				}
				homes.clear();
				homes.putAll(temp_homes);
			}
		}
	}

	public LinkedList<String> getHomeNames() {
		return new LinkedList<>(homes.keySet());
	}

	public int getHomesCount() {
		int count = 0;
		for (String name : homes.keySet()) {
			if (name.equalsIgnoreCase("bed")) {
				continue;
			}
			count++;
		}
		return count;
	}

	public void addDefaultHome(LocationWrapper location) throws SQLException {
		addHome("home", location);
	}

	public void addHome(String name, LocationWrapper location) throws SQLException {
		name = name.toLowerCase();
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			if (!location.isStored()) {
				location.save(connection);
			}
			try (PreparedStatement statement = connection.prepareStatement("REPLACE INTO `homes` (`player_uuid`, `server_id`, `name`, `location_id`) VALUES (UNHEX(?), ?, ?, ?)")) {
				statement.setString(1, Utils.stringFromUuid(getPlayerUuid()));
				statement.setInt(2, SandcastleCore.getInstance().getServerId());
				statement.setString(3, name);
				statement.setInt(4, location.getStoredId());
				statement.execute();
			}
		}
		homes.put(name, location.getStoredId());
	}

	public void removeDefaultHome() throws SQLException, NoSuchHomeException {
		if (homes.size() == 1) {
			for (String name : homes.keySet()) {
				removeHome(name);
			}
			return;
		}
		removeHome("home");
	}

	public void removeHome(String name) throws SQLException, NoSuchHomeException {
		name = name.toLowerCase();
		Integer id = homes.remove(name);
		if (id == null) {
			throw new NoSuchHomeException(name);
		}
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("DELETE FROM `homes` WHERE `player_uuid`=UNHEX(?) AND `name`=? AND `server_id`=?")) {
				statement.setString(1, Utils.stringFromUuid(getPlayerUuid()));
				statement.setString(2, name);
				statement.setInt(3, SandcastleCore.getInstance().getServerId());
			}
			LocationWrapper.deleteLocation(id, connection);
		}
	}

	public int getHomeLocationId(String name) throws NoSuchHomeException {
		name = name.toLowerCase();
		Integer id = homes.get(name);
		if (id == null) {
			throw new NoSuchHomeException(name);
		}
		return id;
	}

	public LocationWrapper getHomeLocation(String name) throws SQLException, NoSuchHomeException {
		return LocationWrapper.fetchLocation(getHomeLocationId(name));
	}

	public int getDefaultHomeLocationId() throws NoSuchHomeException {
		if (homes.size() == 1) {
			for (int id : homes.values()) {
				return id;
			}
		}
		return getHomeLocationId("home");
	}

	public LocationWrapper getDefaultHomeLocation() throws SQLException, NoSuchHomeException {
		return LocationWrapper.fetchLocation(getDefaultHomeLocationId());
	}

	@Override
	public void save(Connection connection) throws SQLException {

	}

	@Override
	public void apply(Player player) {

	}

	@Override
	public Map<String, Object> getMapping() {
		return ImmutableMap.of("homes", ImmutableMap.copyOf(homes));
	}
}
