package com.goldensandsmc.foundation.player.data;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.player.PlayerDataContainer;
import com.goldensandsmc.core.util.Utils;
import com.google.common.collect.ImmutableMap;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.bukkit.entity.Player;

/**
 * @author ShortCircuit908
 *         Created on 6/29/2017.
 */
public class KitUsage extends PlayerDataContainer {
	private final HashMap<String, Date> usage_history = new HashMap<>();

	public KitUsage(UUID player_uuid) {
		super(player_uuid);
	}

	@Override
	public void load(Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT LOWER(`kit_name`), `last_used` FROM `kit_usage` WHERE `player_uuid`=UNHEX(?) AND `server_id`=?")) {
			statement.setString(1, Utils.stringFromUuid(getPlayerUuid()));
			statement.setInt(2, SandcastleCore.getInstance().getServerId());
			try (ResultSet result = statement.executeQuery()) {
				usage_history.clear();
				while (result.next()) {
					usage_history.put(result.getString(1), result.getTimestamp(2));
				}
			}
		}
	}

	@Override
	public void save(Connection connection) throws SQLException {

	}

	@Override
	public void apply(Player player) {

	}

	@Override
	public Map<String, Object> getMapping() {
		return ImmutableMap.of("usage_history", ImmutableMap.copyOf(usage_history));
	}

	public Date getLastUsed(String kit) {
		return usage_history.get(kit);
	}

	public void setLastUsed(String kit, Date used) throws SQLException {
		usage_history.put(kit, used);
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("REPLACE INTO `kit_usage` (`player_uuid`, `server_id`, `kit_name`, `last_used`) VALUES (UNHEX(?), ?, LOWER(?), ?)")) {
				statement.setString(1, Utils.stringFromUuid(getPlayerUuid()));
				statement.setInt(2, SandcastleCore.getInstance().getServerId());
				statement.setString(3, kit);
				statement.setTimestamp(4, new Timestamp(used.getTime()));
				statement.execute();
			}
		}
	}
}
