package com.goldensandsmc.foundation.util;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.NoSuchPlayerException;
import com.goldensandsmc.core.exception.PageOutOfRangeException;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.Utils;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/6/2017.
 */
public class Mailbox {
	public static final int MESSAGES_PER_PAGE = 5;

	public static void sendToAll(UUID sender, String message) throws SQLException {
		send(sender, null, message, true);
	}

	public static void send(UUID sender, UUID recipient, String message) throws SQLException {
		send(sender, recipient, message, false);
	}

	private static void send(UUID sender, UUID recipient, String message, boolean send_to_all) throws SQLException {
		LinkedList<UUID> recipients = new LinkedList<>();
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			if (send_to_all) {
				gatherUuids(connection, recipients);
			}
			else {
				recipients.add(recipient);
			}
			for (UUID recipient_id : recipients) {
				try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `mail` (`sender_uuid`, `recipient_uuid`, `message`) VALUES (UNHEX(?), UNHEX(?), ?)")) {
					statement.setString(1, Utils.stringFromUuid(sender));
					statement.setString(2, Utils.stringFromUuid(recipient_id));
					statement.setString(3, message);
					statement.execute();
				}
				OfflinePlayer player = SandcastleCore.getInstance().getServer().getOfflinePlayer(recipient_id);
				if (player.isOnline()) {
					player.getPlayer().sendMessage(ChatColor.AQUA + "You have a new message! Read it with " + ChatColor.GOLD + "/mail read");
				}
			}
		}
	}

	public static void sendToAll(String sender_name, String message) throws SQLException {
		send(sender_name, null, message, true);
	}

	public static void send(String sender_name, UUID recipient, String message) throws SQLException {
		send(sender_name, recipient, message, false);
	}

	private static void send(String sender_name, UUID recipient, String message, boolean send_to_all) throws SQLException {
		LinkedList<UUID> recipients = new LinkedList<>();
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			if (send_to_all) {
				gatherUuids(connection, recipients);
			}
			else {
				recipients.add(recipient);
			}
			for (UUID recipient_id : recipients) {
				try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `mail` (`sender_name`, `recipient_uuid`, `message`) VALUES (?, UNHEX(?), ?)")) {
					statement.setString(1, sender_name);
					statement.setString(2, Utils.stringFromUuid(recipient_id));
					statement.setString(3, message);
					statement.execute();
				}
				OfflinePlayer player = SandcastleCore.getInstance().getServer().getOfflinePlayer(recipient_id);
				if (player.isOnline()) {
					player.getPlayer().sendMessage(ChatColor.AQUA + "You have a new message! Read it with " + ChatColor.GOLD + "/mail read");
				}
			}
		}
	}

	public static void sendToAll(CommandSender sender, String message) throws SQLException {
		send(sender, null, message, true);
	}

	public static void send(CommandSender sender, UUID recipient, String message) throws SQLException {
		send(sender, recipient, message, false);
	}

	private static void send(CommandSender sender, UUID recipient, String message, boolean send_to_all) throws SQLException {
		if (sender instanceof OfflinePlayer) {
			send(((OfflinePlayer) sender).getUniqueId(), recipient, message, send_to_all);
		}
		else {
			send(sender.getName(), recipient, message, send_to_all);
		}
	}

	private static void gatherUuids(Connection connection, LinkedList<UUID> list) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT HEX(`uuid`) FROM `players`")) {
			try (ResultSet result = statement.executeQuery()) {
				while (result.next()) {
					list.add(Utils.uuidFromString(result.getString(1)));
				}
			}
		}
	}

	public static int getMessageCount(UUID recipient, boolean read) throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM `mail` WHERE `recipient_uuid`=UNHEX(?) AND `read`=?")) {
				statement.setString(1, Utils.stringFromUuid(recipient));
				statement.setInt(2, read ? 1 : 0);
				try (ResultSet result = statement.executeQuery()) {
					int count = 0;
					if (result.next()) {
						count = result.getInt(1);
					}
					return count;
				}
			}
		}
	}

	public static LinkedList<MailMessage> getMessages(UUID recipient, int page, boolean archived) throws CommandException, SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM `mail` WHERE `recipient_uuid`=UNHEX(?) AND `read`=?")) {
				statement.setString(1, Utils.stringFromUuid(recipient));
				statement.setInt(2, archived ? 1 : 0);
				try (ResultSet result = statement.executeQuery()) {
					int total_messages = 0;
					if (result.next()) {
						total_messages = result.getInt(1);
					}
					if (page > total_messages / MESSAGES_PER_PAGE) {
						throw new PageOutOfRangeException(total_messages / MESSAGES_PER_PAGE + 1);
					}
				}
			}
			try (PreparedStatement statement = connection.prepareStatement("SELECT HEX(`sender_uuid`), `sender_name`, `message`, `date_sent`, `read` FROM `mail` WHERE `recipient_uuid`=UNHEX(?) AND `read`=? ORDER BY `date_sent` DESC LIMIT ? OFFSET ?")) {
				statement.setString(1, Utils.stringFromUuid(recipient));
				statement.setInt(2, archived ? 1 : 0);
				statement.setInt(3, MESSAGES_PER_PAGE);
				statement.setInt(4, page * MESSAGES_PER_PAGE);
				try (ResultSet result = statement.executeQuery()) {
					LinkedList<MailMessage> messages = new LinkedList<>();
					while (result.next()) {
						messages.add(new MailMessage(
								Utils.uuidFromString(result.getString(1)),
								result.getString(2),
								recipient,
								result.getString(3),
								result.getTimestamp(4),
								result.getInt(5) != 0
						));
					}
					return messages;
				}
			}
		}
	}

	public static void markAsRead(UUID recipient) throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("UPDATE `mail` SET `read`=1 WHERE `recipient_uuid`=UNHEX(?)")) {
				statement.setString(1, Utils.stringFromUuid(recipient));
				statement.execute();
			}
		}
	}

	public static class MailMessage {
		// 1: sender
		// 2: message
		// 3: date sent
		private static final String MESSAGE_FORMAT =
				"[\"\",{\"text\":\"[\",\"color\":\"aqua\"},{\"text\":\"%1$s\",\"color\":\"gold\",\"clickEvent\":{\"a" +
						"ction\":\"suggest_command\",\"value\":\"/mail send %1$s \"},\"hoverEvent\":{\"action\":\"sh" +
						"ow_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click to reply\",\"color\":\"aqua" +
						"\"}]}}},{\"text\":\"]: \",\"color\":\"aqua\"},{\"text\":\"%2$s\",\"color\":\"gold\",\"hover" +
						"Event\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Sent %3$s" +
						"\",\"color\":\"yellow\"}]}}}]";

		private UUID sender_uuid;
		private String sender_name;
		private UUID recipient_uuid;
		private String message;
		private Date date_sent;
		private boolean read;

		public MailMessage(UUID sender_uuid, String sender_name, UUID recipient_uuid, String message, Date date_sent, boolean read) {
			this.sender_uuid = sender_uuid;
			this.sender_name = sender_name;
			this.recipient_uuid = recipient_uuid;
			this.message = message;
			this.date_sent = date_sent;
			this.read = read;
		}

		public UUID getSenderUuid() {
			return sender_uuid;
		}

		public String getSenderName() {
			if (sender_name == null && sender_uuid != null) {
				try {
					sender_name = PlayerManager.getOrCreate(sender_uuid).getValue().getDisplayName();
				}
				catch (NoSuchPlayerException | SQLException e) {
					sender_name = "UNKNOWN";
					e.printStackTrace();
				}
			}
			return sender_name;
		}

		public UUID getRecipientUuid() {
			return recipient_uuid;
		}

		public String getMessage() {
			return message;
		}

		public Date getDateSent() {
			return date_sent;
		}

		public boolean isRead() {
			return read;
		}

		public String toTellrawString() {
			return String.format(MESSAGE_FORMAT, getSenderName(), getMessage(), DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(getDateSent()));
		}
	}
}
