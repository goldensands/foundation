package com.goldensandsmc.foundation.util;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.PlayerNotOnlineException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.foundation.SandcastleFoundation;
import com.goldensandsmc.foundation.player.data.KitUsage;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.inventory.ItemStack;

/**
 * @author ShortCircuit908
 *         Created on 6/29/2017.
 */
public class Kit {
	private static final HashMap<String, Kit> kits = new HashMap<>();

	private final int cooldown;
	private final ItemStack[] items;

	public Kit(int cooldown, ItemStack... items) {
		this.cooldown = cooldown;
		this.items = items;
	}

	public int getCooldown() {
		return cooldown;
	}

	public ItemStack[] getItems() {
		return items;
	}

	public static boolean canUse(String kit_name, PlayerContainer target) {
		kit_name = kit_name.toLowerCase();
		if (!target.isOnline()) {
			return false;
		}
		try {
			if (target.getPlayer().hasPermission("sandcastle.foundation.kit.cooldown.bypass")) {
				return true;
			}
			if(!target.getPlayer().hasPermission("sandcastle.foundation.kit." + kit_name)){
				return false;
			}
		}
		catch (PlayerNotOnlineException e) {
			// Should never happen
			e.printStackTrace();
		}
		long now = System.currentTimeMillis();
		Kit kit = kits.get(kit_name);
		KitUsage usage = target.getDataContainer(KitUsage.class);
		Date last_used = usage.getLastUsed(kit_name);
		if (last_used == null) {
			return kit.getCooldown() >= 0;
		}
		return (last_used.getTime() - now) / 1000 >= kit.getCooldown();
	}

	public static int getSecondsBeforeCanUse(String kit_name, PlayerContainer target) {
		if (!target.isOnline()) {
			return -1;
		}
		try {
			if (target.getPlayer().hasPermission("sandcastle.foundation.kit.cooldown.bypass")) {
				return 0;
			}
		}
		catch (PlayerNotOnlineException e) {
			// Should never happen
			e.printStackTrace();
		}
		kit_name = kit_name.toLowerCase();
		long now = System.currentTimeMillis();
		Kit kit = kits.get(kit_name);
		KitUsage usage = target.getDataContainer(KitUsage.class);
		Date last_used = usage.getLastUsed(kit_name);
		return (int) ((last_used.getTime() - now) / 1000 - kit.getCooldown());
	}

	public static void loadKits() {
		Type type = new TypeToken<HashMap<String, Kit>>() {
		}.getType();
		Map<String, Kit> new_kits = SandcastleFoundation.getInstance().getJsonConfig().getNode("kits", type, new HashMap<>());
		kits.clear();
		for (Map.Entry<String, Kit> entry : new_kits.entrySet()) {
			kits.put(entry.getKey().toLowerCase(), entry.getValue());
		}
	}

	public static void saveKits() {
		SandcastleCore.getInstance().getJsonConfig().setNode("kits", new HashMap<>(kits));
		SandcastleCore.getInstance().getJsonConfig().saveConfig();
	}

	public static HashMap<String, Kit> getKits() {
		return kits;
	}
}
