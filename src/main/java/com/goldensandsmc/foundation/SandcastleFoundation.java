package com.goldensandsmc.foundation;

import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.JsonConfig;
import com.goldensandsmc.foundation.commands.BigTreeCommand;
import com.goldensandsmc.foundation.commands.DelhomeCommand;
import com.goldensandsmc.foundation.commands.EnchantCommand;
import com.goldensandsmc.foundation.commands.ExperienceCommand;
import com.goldensandsmc.foundation.commands.FeedCommand;
import com.goldensandsmc.foundation.commands.FlyCommand;
import com.goldensandsmc.foundation.commands.FromJsonCommand;
import com.goldensandsmc.foundation.commands.GamemodeCommand;
import com.goldensandsmc.foundation.commands.GiveCommand;
import com.goldensandsmc.foundation.commands.GodCommand;
import com.goldensandsmc.foundation.commands.HatCommand;
import com.goldensandsmc.foundation.commands.HealCommand;
import com.goldensandsmc.foundation.commands.HomeCommand;
import com.goldensandsmc.foundation.commands.KitCommand;
import com.goldensandsmc.foundation.commands.MailCommand;
import com.goldensandsmc.foundation.commands.MetaCommand;
import com.goldensandsmc.foundation.commands.MoreCommand;
import com.goldensandsmc.foundation.commands.PlayerTimeCommand;
import com.goldensandsmc.foundation.commands.SethomeCommand;
import com.goldensandsmc.foundation.commands.ToJsonCommand;
import com.goldensandsmc.foundation.commands.TreeCommand;
import com.goldensandsmc.foundation.commands.WhoisCommand;
import com.goldensandsmc.foundation.listeners.PlayerListener;
import com.goldensandsmc.foundation.player.data.Homes;
import com.goldensandsmc.foundation.player.data.KitUsage;
import com.goldensandsmc.foundation.util.Kit;
import com.shortcircuit.utils.bukkit.command.CommandRegister;
import java.io.File;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author ShortCircuit908
 *         Created on 6/23/2017.
 */
public class SandcastleFoundation extends JavaPlugin {
	private static SandcastleFoundation instance;
	private final JsonConfig config;

	public SandcastleFoundation() {
		SandcastleFoundation.instance = this;
		this.config = new JsonConfig(this, new File(getDataFolder() + File.separator + "config.json"), "config.json");
		this.config.loadConfig();
	}

	public static SandcastleFoundation getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {
		if (!getServer().getPluginManager().getPlugin("SandcastleCore").isEnabled()) {
			getLogger().warning("SandcastleCore is not enabled.");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		/* Commands */
		CommandRegister.register(new BigTreeCommand(this));
		CommandRegister.register(new DelhomeCommand(this));
		CommandRegister.register(new EnchantCommand(this));
		CommandRegister.register(new ExperienceCommand(this));
		CommandRegister.register(new FeedCommand(this));
		CommandRegister.register(new FlyCommand(this));
		CommandRegister.register(new FromJsonCommand(this));
		CommandRegister.register(new GamemodeCommand(this));
		CommandRegister.register(new GiveCommand(this));
		CommandRegister.register(new GodCommand(this));
		CommandRegister.register(new HatCommand(this));
		CommandRegister.register(new HealCommand(this));
		CommandRegister.register(new HomeCommand(this));
		CommandRegister.register(new KitCommand(this));
		CommandRegister.register(new MailCommand(this));
		CommandRegister.register(new MetaCommand(this));
		CommandRegister.register(new MoreCommand(this));
		CommandRegister.register(new PlayerTimeCommand(this));
		CommandRegister.register(new SethomeCommand(this));
		CommandRegister.register(new ToJsonCommand(this));
		CommandRegister.register(new TreeCommand(this));
		CommandRegister.register(new WhoisCommand(this));

		/* Data containers */
		PlayerManager.registerDataContainer(Homes.class);
		PlayerManager.registerDataContainer(KitUsage.class);

		/* Listeners */
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);

		Kit.loadKits();
	}

	public JsonConfig getJsonConfig() {
		return config;
	}
}
