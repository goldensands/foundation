package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.util.Utils;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
public class TreeCommand extends CoreCommand<Player> {
	private static final HashMap<String, TreeType> tree_types = new HashMap<>();

	static {
		tree_types.put("oak", TreeType.TREE);
		tree_types.put("birch", TreeType.BIRCH);
		tree_types.put("tallbirch", TreeType.TALL_BIRCH);
		tree_types.put("redwood", TreeType.REDWOOD);
		tree_types.put("tallredwood", TreeType.TALL_REDWOOD);
		tree_types.put("redmushroom", TreeType.RED_MUSHROOM);
		tree_types.put("brownmushroom", TreeType.BROWN_MUSHROOM);
		tree_types.put("jungle", TreeType.SMALL_JUNGLE);
		tree_types.put("junglebush", TreeType.JUNGLE_BUSH);
		tree_types.put("cocoa", TreeType.COCOA_TREE);
		tree_types.put("swamp", TreeType.SWAMP);
		tree_types.put("acacia", TreeType.ACACIA);
		tree_types.put("darkoak", TreeType.DARK_OAK);
	}

	public TreeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "tree";
	}

	@Override
	public String[] getCommandAliases() {
		return null;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Spawns a tree"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <" + Joiner.on('|').join(tree_types.keySet()) + ">"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.tree";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.size() == 0) {
			throw new TooFewArgumentsException();
		}
		TreeType tree_type = tree_types.get(args.get(0));
		if (tree_type == null) {
			throw new InvalidArgumentException("Expected one of [" + Joiner.on(", ").join(tree_types.keySet()) + "]");
		}
		Block block = Utils.getTargetBlock(sender, 32, true, true);
		if (block == null) {
			throw new CommandException("You are not looking at a solid block");
		}
		if (!sender.getWorld().generateTree(block.getLocation().add(0.0, 1.0, 0.0), tree_type)) {
			throw new CommandException("Failed to generate tree");
		}
		return new String[]{"Poof!"};
	}
}
