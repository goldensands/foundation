package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public class MoreCommand extends CoreCommand<Player> {

	public MoreCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "more";
	}

	@Override
	public String[] getCommandAliases() {
		return null;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Fills out an item to the max stack size"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[0];
	}

	@Override
	public String getCommandPermission() {
		return null;
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		ItemStack item = sender.getItemInHand();
		if (item == null || item.getType().name().equalsIgnoreCase("air")) {
			throw new CommandException("You are not holding an item");
		}
		int amount = sender.hasPermission(getCommandPermission() + ".oversized") ? 64 : item.getMaxStackSize();
		item.setAmount(amount);
		return new String[]{"Poof!"};
	}
}
