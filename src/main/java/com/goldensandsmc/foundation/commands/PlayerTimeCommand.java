package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.Utils;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public class PlayerTimeCommand extends CoreCommand<CommandSender> {

	public PlayerTimeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "ptime";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"playertime"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Change a player's time"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player] [@][reset|dawn|noon|dusk|midnight|day|night|4:30pm|16:30|10500ticks]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.ptime";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.isEmpty()) {
			if (!(sender instanceof Player)) {
				throw new TooFewArgumentsException();
			}
			target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
		}
		else {
			if (!sender.hasPermission(getCommandPermission() + ".others")) {
				throw new NoPermissionException(getCommandPermission() + ".others");
			}
			target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
		}
		if (args.isEmpty()) {
			long ticks = target.getPlayer().getPlayerTimeOffset();
			String time_string = Utils.stringFromGameTime(ticks);
			return new String[]{(target.getPlayer().equals(sender) ? "Your " : ChatColor.GOLD + target.getDisplayName()
					+ ChatColor.AQUA + "'s") + " game time is " + ChatColor.GOLD + time_string + ChatColor.AQUA + " or "
					+ ChatColor.GOLD + "ticks"};
		}
		String time_string = args.get(0);
		if (time_string.equalsIgnoreCase("reset")) {
			target.getPlayer().resetPlayerTime();
			return new String[]{(target.getPlayer().equals(sender) ? "Your " : ChatColor.GOLD + target.getDisplayName()
					+ ChatColor.AQUA + "'s") + " game time has been reset"};
		}
		boolean absolute = time_string.startsWith("@");
		if (absolute) {
			time_string = time_string.substring(1);
		}
		long ticks = Utils.gameTimeFromString(time_string);
		target.getPlayer().setPlayerTime(ticks, !absolute);
		return new String[]{(target.getPlayer().equals(sender) ? "Your " : ChatColor.GOLD + target.getDisplayName()
				+ ChatColor.AQUA + "'s") + " game time has been set to " + ChatColor.GOLD + time_string + ChatColor.AQUA
				+ " or " + ChatColor.GOLD + "ticks"};
	}
}
