package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.util.EnchantmentHelper;
import com.goldensandsmc.core.util.RomanNumerals;
import com.goldensandsmc.core.util.Utils;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
public class EnchantCommand extends CoreCommand<Player> {

	public EnchantCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "enchant";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"enchantment"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Enchants the held item"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <enchantment> [level] [force]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.enchant";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.size() == 0) {
			Enchantment[] enchantments = EnchantmentHelper.getAllEnchantments();
			LinkedList<String> names = new LinkedList<>();
			for (Enchantment enchantment : enchantments) {
				names.add(enchantment.getName());
			}
			Collections.sort(names);
			return new String[]{"Available enchantments: " + ChatColor.GOLD + Joiner.on(ChatColor.AQUA + ", " + ChatColor.GOLD).join(names)};
		}
		ItemStack target = sender.getItemInHand();
		if (target == null) {
			throw new CommandException("You are not holding an item");
		}
		Enchantment enchantment = null;
		String enchantment_name = args.get(0);
		Integer enchantment_id = null;
		int level = 1;
		boolean force = false;
		try {
			enchantment_id = Integer.parseInt(enchantment_name);
		}
		catch (NumberFormatException e) {
			// Do nothing
		}
		for (Enchantment check : EnchantmentHelper.getAllEnchantments()) {
			if (check.getName().equalsIgnoreCase(enchantment_name) || (enchantment_id != null && enchantment_id == check.getId())) {
				enchantment = check;
				break;
			}
		}
		if (enchantment == null) {
			if (enchantment_id == null) {
				throw new InvalidArgumentException("Unknown enchantment name: " + enchantment_name);
			}
			throw new InvalidArgumentException("Unknown enchantment ID: " + enchantment_id);
		}
		if(!sender.hasPermission(getCommandPermission() + ".enchantments." + enchantment.getName().toLowerCase())){
			throw new InvalidArgumentException("You do not have permission to use this enchantment");
		}
		if (args.size() > 1) {
			try {
				level = Integer.parseInt(args.get(1));
			}
			catch (NumberFormatException e) {
				try{
					level = (int) RomanNumerals.romanNumeralsToNumber(args.get(1));
				}
				catch (NumberFormatException e1){
					throw new InvalidArgumentException("Cannot parse number: " + args.get(1));
				}
			}
		}
		if (args.size() > 2) {
			force = args.get(2).toLowerCase().matches("1|true|yes|force|y");
		}
		if (force && !sender.hasPermission(getCommandPermission() + ".allowunsafe")) {
			throw new NoPermissionException(getCommandPermission() + ".allowunsafe");
		}
		if (!force) {
			if (!enchantment.canEnchantItem(target)) {
				throw new InvalidArgumentException(Utils.getFriendlyName(enchantment.getName()) + " cannot be applied to "
						+ Utils.getFriendlyName(target.getType().name()));
			}
			for (Enchantment check : target.getEnchantments().keySet()) {
				if (check.conflictsWith(enchantment) || enchantment.conflictsWith(check)) {
					throw new InvalidArgumentException("Enchantment conflicts with " + check.getName());
				}
			}
			if (level < enchantment.getStartLevel()) {
				throw new InvalidArgumentException("Level is too low (min " + enchantment.getStartLevel() + ")");
			}
			if (level > enchantment.getMaxLevel()) {
				throw new InvalidArgumentException("Level is too high (max " + enchantment.getMaxLevel() + ")");
			}
		}
		if (level > Short.MAX_VALUE) {
			throw new InvalidArgumentException("Level is too high (max " + Short.MAX_VALUE + ")");
		}
		if (level < Short.MIN_VALUE) {
			throw new InvalidArgumentException("Level is too low (min " + Short.MIN_VALUE + ")");
		}
		target.addUnsafeEnchantment(enchantment, level);
		return new String[]{"Added enchantment " + ChatColor.GOLD + Utils.getFriendlyName(enchantment.getName())
				+ ChatColor.AQUA + " lvl " + ChatColor.GOLD + level + ChatColor.AQUA + " to " + ChatColor.GOLD +
				Utils.getFriendlyName(target.getType().name())};
	}
}
