package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.Utils;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/14/2017.
 */
public class GamemodeCommand extends CoreCommand<CommandSender> {

	public GamemodeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "gamemode";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"gm"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Changes a player's gamemode"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player] <" + Joiner.on(", ").join(GameMode.values()) + ">"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.gamemode";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.isEmpty()) {
			throw new TooFewArgumentsException();
		}
		if (args.size() == 1) {
			if (!(sender instanceof Player)) {
				throw new TooFewArgumentsException();
			}
			target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
		}
		else {
			if (!sender.hasPermission(getCommandPermission() + ".others")) {
				throw new NoPermissionException(getCommandPermission() + ".others");
			}
			target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
		}
		if (args.isEmpty()) {
			throw new TooFewArgumentsException();
		}
		String raw_gamemode = args.get(0).toUpperCase();
		GameMode gamemode;
		try {
			gamemode = getGameMode(raw_gamemode);
		}
		catch (IllegalArgumentException e) {
			throw new InvalidArgumentException("Expected one of [" + Joiner.on(", ").join(GameMode.values()) + "]");
		}
		if (!sender.hasPermission(getCommandPermission() + ".mode." + gamemode.name().toLowerCase())) {
			throw new InvalidArgumentException("You do not have permission to use that gamemode");
		}
		PlayerGameModeChangeEvent event = new PlayerGameModeChangeEvent(target.getPlayer(), gamemode);
		getPlugin().getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return new String[]{"Failed to set " + (sender.equals(target.getPlayer()) ? "your" : ChatColor.GOLD
					+ target.getDisplayName() + ChatColor.AQUA + "'s") + " gamemode"};
		}
		target.getPlayer().setGameMode(gamemode);
		if (!sender.equals(target.getPlayer())) {
			target.getPlayer().sendMessage(ChatColor.AQUA + "Set your gamemode to " + ChatColor.GOLD + Utils.getFriendlyName(gamemode.name()));
		}
		return new String[]{"Set " + (sender.equals(target.getPlayer()) ? "your" : ChatColor.GOLD + target.getDisplayName()
				+ ChatColor.AQUA + "'s") + " gamemode to " + ChatColor.GOLD + Utils.getFriendlyName(gamemode.name())};
	}

	private static GameMode getGameMode(String name) throws IllegalArgumentException {
		name = name.toLowerCase();
		try {
			int id = Integer.parseInt(name);
			for (GameMode gamemode : GameMode.values()) {
				if (gamemode.getValue() == id) {
					return gamemode;
				}
			}
			throw new IllegalArgumentException();
		}
		catch (NumberFormatException e) {
			// Do nothing
		}
		for (GameMode gamemode : GameMode.values()) {
			if (gamemode.name().equalsIgnoreCase(name) || gamemode.name().toLowerCase().startsWith(name)) {
				return gamemode;
			}
		}
		throw new IllegalArgumentException();
	}
}
