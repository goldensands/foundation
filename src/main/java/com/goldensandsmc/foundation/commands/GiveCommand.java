package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.ItemStackTypeAdapter;
import com.goldensandsmc.core.util.Utils;
import com.goldensandsmc.foundation.SandcastleFoundation;
import com.google.common.base.Joiner;
import com.google.gson.JsonParser;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.PlayerOnlyException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/19/2017.
 */
public class GiveCommand extends CoreCommand<CommandSender> {

	public GiveCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "give";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"item", "i"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Give a player an item"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player] <item>[:data] [amount]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.give";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.isEmpty()) {
			throw new TooFewArgumentsException();
		}
		PlayerContainer target;
		switch (command.toLowerCase()) {
			case "i":
			case "item":
				if (!(sender instanceof Player)) {
					throw new PlayerOnlyException();
				}
				target = PlayerManager.getOrCreate(((Player) sender).getUniqueId()).getValue();
				break;
			default:
				target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
				break;
		}
		int amount = SandcastleFoundation.getInstance().getJsonConfig().getNode("commands.give.default_amount", int.class, 1);
		ItemStack item = Utils.getItemFromString(args.remove(0));
		if (!args.isEmpty()) {
			try {
				amount = Integer.parseInt(args.remove(0));
			}
			catch (NumberFormatException e) {
				throw new InvalidArgumentException(e);
			}
		}
		item.setAmount(amount);
		if (!args.isEmpty()) {
			if (sender.hasPermission(getCommandPermission() + ".meta")) {
				String raw_meta = Joiner.on(' ').join(args);
				ItemMeta meta = null;
				try {
					meta = ItemStackTypeAdapter.deserializeMeta(new JsonParser().parse(raw_meta), item.getType());
				}
				catch (Exception e) {
					sender.sendMessage(ChatColor.RED + "Unable to parse metadata: " + e.getMessage());
				}
				item.setItemMeta(meta);
			}
			else {
				sender.sendMessage(ChatColor.RED + "You do not have permission to spawn items with metadata");
			}
		}
		for (ItemStack overflow : target.getPlayer().getInventory().addItem(item).values()) {
			target.getPlayer().getWorld().dropItem(target.getPlayer().getLocation().add(0, 0.25, 0), overflow).setPickupDelay(0);
		}
		return new String[]{"Gave " + ChatColor.GOLD + item.getAmount() + ChatColor.AQUA + " of " + ChatColor.GOLD
				+ Utils.getFriendlyName(item.getType().name()) + (item.getDurability() == 0 ? "" : ":" + item.getDurability())
				+ (target.getPlayer().equals(sender) ? "" : ChatColor.AQUA + " to " + ChatColor.GOLD + target.getDisplayName())};
	}
}
