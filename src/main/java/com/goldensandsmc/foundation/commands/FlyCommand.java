package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/14/2017.
 */
public class FlyCommand extends CoreCommand<CommandSender> {

	public FlyCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "fly";
	}

	@Override
	public String[] getCommandAliases() {
		return null;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[0];
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player [on|off]]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.fly";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.isEmpty()) {
			if (!(sender instanceof Player)) {
				throw new TooFewArgumentsException();
			}
			target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
		}
		else {
			if (!sender.hasPermission(getCommandPermission() + ".others")) {
				throw new NoPermissionException(getCommandPermission() + ".others");
			}
			target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
		}
		boolean allow = !target.getPlayer().getAllowFlight();
		if (!args.isEmpty()) {
			String value = args.get(0);
			if (!value.matches("on|off")) {
				throw new InvalidArgumentException("Expected one of [on, off]");
			}
			allow = value.equalsIgnoreCase("on");
		}
		target.getPlayer().setAllowFlight(allow);
		target.getPlayer().setFallDistance(0);
		if (!allow) {
			target.getPlayer().setFlying(false);
		}
		String message = "Set fly mode " + ChatColor.GOLD + (allow ? "enabled" : "disabled") + ChatColor.AQUA + " for "
				+ ChatColor.GOLD + target.getDisplayName();
		if (!sender.equals(target.getPlayer())) {
			target.getPlayer().sendMessage(ChatColor.AQUA + message);
		}
		return new String[]{message};
	}
}
