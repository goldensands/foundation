package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.foundation.player.data.Homes;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class DelhomeCommand extends CoreCommand<Player> {

	public DelhomeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "delhome";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"remhome", "rmhome"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Deletes a home"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> [player:][home]",
		};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.delhome";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.size() == 0) {
			target = PlayerManager.getOrCreate(sender.getUniqueId()).getValue();
			target.getDataContainer(Homes.class).removeDefaultHome();
		}
		else {
			String[] parts = args.get(0).split(":", 2);
			if (parts.length > 1) {
				if (!sender.hasPermission(getCommandPermission() + ".others")) {
					throw new NoPermissionException(getCommandPermission() + ".others");
				}
				target = PlayerManager.getOrCreateByAnyName(parts[0]).getValue();
			}
			else {
				target = PlayerManager.getOrCreate(sender.getUniqueId()).getValue();
			}
			String home_name = parts[parts.length - 1];
			target.getDataContainer(Homes.class).removeHome(home_name);
		}
		return new String[]{"Home removed!"};
	}
}
