package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public class HatCommand extends CoreCommand<Player> {

	public HatCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "hat";
	}

	@Override
	public String[] getCommandAliases() {
		return null;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Swap the item in hand for the item on your head"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.hat";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.isEmpty()) {
			target = PlayerManager.getOrCreate(sender).getValue();
		}
		else {
			if (!sender.hasPermission(getCommandPermission() + ".others")) {
				throw new NoPermissionException(getCommandPermission() + ".others");
			}
			target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
		}
		ItemStack item = sender.getItemInHand();
		if (item == null || item.getType().name().equalsIgnoreCase("air")) {
			throw new CommandException("You are not holding an item");
		}
		Player player = target.getPlayer();
		ItemStack current_hat = player.getInventory().getHelmet();
		player.getInventory().setHelmet(item);
		player.setItemInHand(current_hat);
		return new String[]{"Enjoy your new hat!"};
	}
}
