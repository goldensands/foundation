package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.util.Utils;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
public class BigTreeCommand extends CoreCommand<Player> {
	private static final HashMap<String, TreeType> large_tree_types = new HashMap<>();

	static {
		large_tree_types.put("oak", TreeType.BIG_TREE);
		large_tree_types.put("redwood", TreeType.MEGA_REDWOOD);
		large_tree_types.put("jungle", TreeType.JUNGLE);
	}

	public BigTreeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "bigtree";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"largetree"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Spawns a large tree"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <" + Joiner.on('|').join(large_tree_types.keySet()) + ">"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.bigtree";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.size() == 0) {
			throw new TooFewArgumentsException();
		}
		TreeType tree_type = large_tree_types.get(args.get(0));
		if (tree_type == null) {
			throw new InvalidArgumentException("Expected one of [" + Joiner.on(", ").join(large_tree_types.keySet()) + "]");
		}
		Block block = Utils.getTargetBlock(sender, 32, true, true);
		if (block == null) {
			throw new CommandException("You are not looking at a solid block");
		}
		if (!sender.getWorld().generateTree(block.getLocation().add(0.0, 1.0, 0.0), tree_type)) {
			throw new CommandException("Failed to generate tree");
		}
		return new String[]{"Poof!"};
	}
}
