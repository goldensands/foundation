package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.util.ReflectionHelper;
import com.goldensandsmc.core.util.ReflectionParser;
import com.goldensandsmc.core.util.Utils;
import com.google.common.base.Joiner;
import com.google.gson.JsonParseException;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.SQLException;
import java.util.LinkedList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/21/2017.
 */
public class MetaCommand extends CoreCommand<Player> {

	public MetaCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "meta";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"itemmeta", "imeta", "editmeta", "edititemmeta", "editimeta"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Modifies the metadata of the item in hand"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> fields",
				"/<command> set <field:value[; field:value]...>"
		};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.meta";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		ItemStack item = sender.getItemInHand();
		ItemMeta meta;
		if (checkSubcommand("fields", args)) {
			if (item == null || !item.hasItemMeta()) {
				meta = getPlugin().getServer().getItemFactory().getItemMeta(Material.STONE);
			}
			else {
				meta = item.getItemMeta();
			}
			Field[] fields = ReflectionHelper.getAllFields(meta.getClass());
			LinkedList<String> names = new LinkedList<>();
			for (Field field : fields) {
				if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers())) {
					names.add(field.getName() + "(" + Utils.getFriendlyClassName(field.getType()) + ")");
				}
			}
			return new String[]{"Available meta fields: " + ChatColor.GOLD + Joiner.on(ChatColor.AQUA + ", " + ChatColor.GOLD).join(names)};
		}
		if (checkSubcommand("set", args)) {
			if (item == null || item.getType().name().equalsIgnoreCase("air")) {
				throw new CommandException("You are not holding an item");
			}
			try {
				meta = item.hasItemMeta() ? item.getItemMeta() : Bukkit.getServer().getItemFactory().getItemMeta(item.getType());
				new ReflectionParser(meta, Joiner.on(' ').join(args)).parse().apply(sender);
				item.setItemMeta(meta);
				return new String[]{"Set item meta"};
			}
			catch (JsonParseException e) {
				throw new InvalidArgumentException(e.getMessage());
			}
		}
		throw new InvalidArgumentException("Unknown subcommand. Try /help " + command);
	}
}
