package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.foundation.util.Kit;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/29/2017.
 */
public class KitCommand extends CoreCommand<CommandSender> {

	public KitCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "kit";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"kits"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Spawn a predefined kit"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [kit] [player]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.kit";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.isEmpty()) {
			if (!(sender instanceof Player)) {
				throw new TooFewArgumentsException();
			}
			target = PlayerManager.getOrCreate(((Player) sender).getUniqueId()).getValue();
			LinkedList<String> available_kits = new LinkedList<>();
			for (Map.Entry<String, Kit> entry : Kit.getKits().entrySet()) {
				if (!sender.hasPermission("sandcastle.foundation.kits." + entry.getKey().toLowerCase())) {
					continue;
				}
				if (Kit.canUse(entry.getKey(), target)) {
					available_kits.add(entry.getKey());
				}
			}
		}
		return new String[0];
	}
}
