package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CooldownCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.LocationWrapper;
import com.goldensandsmc.core.util.Utils;
import com.goldensandsmc.foundation.SandcastleFoundation;
import com.goldensandsmc.foundation.player.data.Homes;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class HomeCommand extends CooldownCommand<Player> {

	public HomeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public int getCooldownSeconds() {
		return SandcastleFoundation.getInstance().getJsonConfig().getNode("commands.home.cooldown_seconds", int.class, 0);
	}

	@Override
	public String getCommandName() {
		return "home";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"homes"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Teleport home"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> [player:][home]",
		};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.home";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.size() == 0) {
			target = PlayerManager.getOrCreate(sender.getUniqueId()).getValue();
			if (command.equalsIgnoreCase("homes")) {
				return new String[]{ChatColor.AQUA + "Your homes: " + ChatColor.GOLD + Joiner.on(ChatColor.AQUA + ", "
						+ ChatColor.GOLD).join(target.getDataContainer(Homes.class).getHomeNames())};
			}
			LocationWrapper location = target.getDataContainer(Homes.class).getDefaultHomeLocation();
			Utils.teleportWithLeashed(sender, location.toLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
		}
		else {
			if (command.equalsIgnoreCase("homes")) {
				if (!sender.hasPermission(getCommandPermission() + ".others")) {
					throw new NoPermissionException(getCommandPermission() + ".others");
				}
				target = PlayerManager.getOrCreateByAnyName(args.get(0)).getValue();
				return new String[]{ChatColor.AQUA + target.getDisplayName() + "'s homes: " + ChatColor.GOLD
						+ Joiner.on(ChatColor.AQUA + ", " + ChatColor.GOLD).join(target.getDataContainer(Homes.class).getHomeNames())};
			}
			String[] parts = args.get(0).split(":", 2);
			if (parts.length > 1) {
				if (!sender.hasPermission(getCommandPermission() + ".others")) {
					throw new NoPermissionException(getCommandPermission() + ".others");
				}
				target = PlayerManager.getOrCreateByAnyName(parts[0]).getValue();
			}
			else {
				target = PlayerManager.getOrCreate(sender.getUniqueId()).getValue();
			}
			String home_name = parts[parts.length - 1];
			LocationWrapper location = target.getDataContainer(Homes.class).getHomeLocation(home_name);
			Utils.teleportWithLeashed(sender, location.toLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
		}
		return new String[]{"Welcome home!"};
	}
}
