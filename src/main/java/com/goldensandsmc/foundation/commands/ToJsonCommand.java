package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.util.typeadapters.ItemStackTypeAdapter;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public class ToJsonCommand extends CoreCommand<Player> {

	public ToJsonCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "tojson";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"itemtojson", "jsonfromitem", "fromitem"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Serialize a held item to JSON and print the result to the console"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [meta]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.tojson";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		Object target = sender.getItemInHand();
		if (!args.isEmpty() && args.get(0).equalsIgnoreCase("meta")) {
			if (target == null) {
				throw new CommandException("You are not holding an item");
			}
			target = ((ItemStack) target).getItemMeta();
		}
		String json = ItemStackTypeAdapter.gsonBuilderWithItemSerializers().create().toJson(target);
		getPlugin().getLogger().info(json);
		return new String[]{json};
	}
}
