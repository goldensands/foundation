package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.exception.ContentTooLongException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.foundation.util.Mailbox;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.PlayerOnlyException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import java.util.LinkedList;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/6/2017.
 */
public class MailCommand extends CoreCommand<CommandSender> {

	public MailCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "mail";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"email"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Send and receive mail"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> send <to> <message>",
				"/<command> sendall <message>",
				"/<command> <read|archive> [page]",
				"/<command> clear",
		};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.mail";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.size() == 0) {
			throw new TooFewArgumentsException();
		}
		if (checkSubcommand("send", args)) {
			if (!sender.hasPermission(getCommandPermission() + ".send")) {
				throw new NoPermissionException(getCommandPermission() + ".send");
			}
			if (args.size() < 2) {
				throw new TooFewArgumentsException();
			}
			PlayerContainer recipient = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
			String message = Joiner.on(' ').join(args);
			if (message.length() > 256) {
				throw new ContentTooLongException(256);
			}
			Mailbox.send(sender, recipient.getUuid(), message);
			return new String[]{"Mail sent!"};
		}
		if (checkSubcommand("sendall", args)) {
			if (!sender.hasPermission(getCommandPermission() + ".sendall")) {
				throw new NoPermissionException(getCommandPermission() + ".sendall");
			}
			if (args.size() < 1) {
				throw new TooFewArgumentsException();
			}
			String message = Joiner.on(' ').join(args);
			if (message.length() > 256) {
				throw new ContentTooLongException(256);
			}
			Mailbox.sendToAll(sender, message);
			return new String[]{"Mail sent!"};
		}
		if (!(sender instanceof Player)) {
			throw new PlayerOnlyException();
		}
		Player player = (Player) sender;
		if (checkSubcommand("read", args)) {
			showMail(player, args, false);
			return new String[]{"Use " + ChatColor.GOLD + "/mail clear" + ChatColor.AQUA + " to mark mail as read"};
		}
		if (checkSubcommand("archive", args)) {
			showMail(player, args, true);
			return null;
		}
		if (checkSubcommand("clear", args)) {
			Mailbox.markAsRead(player.getUniqueId());
			return new String[]{"Mail cleared"};
		}
		throw new InvalidArgumentException("Unknown subcommand. Try /help " + command);
	}

	private void showMail(Player player, ConcurrentArrayList<String> args, boolean read) throws SQLException, CommandException {
		int messages_count;
		if ((messages_count = Mailbox.getMessageCount(player.getUniqueId(), read)) == 0) {
			throw new CommandException(ChatColor.AQUA + "You have no " + (read ? "archived" : "new") + " mail");
		}
		int page = 0;
		if (args.size() > 0) {
			try {
				page = Integer.parseInt(args.get(0)) - 1;
			}
			catch (NumberFormatException e) {
				throw new InvalidArgumentException("Cannot parse number: " + args.get(0));
			}
		}
		if (page < 0) {
			throw new InvalidArgumentException("Page cannot be less than 1");
		}
		LinkedList<Mailbox.MailMessage> messages = Mailbox.getMessages(player.getUniqueId(), page, read);
		player.sendMessage(ChatColor.AQUA + "Your " + (read ? "archived " : "") + "mailbox (page " + ChatColor.GOLD + (page + 1) + ChatColor.AQUA
				+ "/" + ChatColor.GOLD + (messages_count / Mailbox.MESSAGES_PER_PAGE + 1) + ChatColor.AQUA + ")");
		for (Mailbox.MailMessage message : messages) {
			BaseComponent[] components = ComponentSerializer.parse(message.toTellrawString());
			TextComponent component = new TextComponent(components);
			player.spigot().sendMessage(component);
		}
	}
}
