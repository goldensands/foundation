package com.goldensandsmc.foundation.commands;

import com.github.jikoo.experience.Experience;
import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
public class ExperienceCommand extends CoreCommand<CommandSender> {

	public ExperienceCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "exp";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"experience", "xp"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Give, set, or view a player's experience"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <show|set|give> [player] [amount|L<level>]"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.exp";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (checkSubcommand("show", args) || checkSubcommand("view", args)) {
			if (args.size() == 0) {
				if (!(sender instanceof Player)) {
					throw new TooFewArgumentsException();
				}
				target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
			}
			else {
				if (!sender.hasPermission(getCommandPermission() + ".others")) {
					throw new NoPermissionException(getCommandPermission() + ".others");
				}
				target = PlayerManager.getOrCreateByAnyName(args.get(0)).getValue();
			}
			int player_exp = Experience.getExp(target.getPlayer());
			int player_level = (int) Experience.getLevelFromExp(player_exp);
			int exp_to_next = Experience.getExpToNext(player_level);
			exp_to_next -= (Experience.getExp(target.getPlayer()) - Experience.getExpFromLevel(player_level));
			return new String[]{ChatColor.GOLD + target.getDisplayName() + ChatColor.AQUA + " has " + ChatColor.GOLD
					+ player_exp + ChatColor.AQUA + " exp (level " + ChatColor.GOLD
					+ (int) Experience.getLevelFromExp(Experience.getExp(target.getPlayer())) + ChatColor.AQUA
					+ ") and needs " + ChatColor.GOLD + exp_to_next + ChatColor.AQUA + " more exp to level up"};
		}
		if (checkSubcommand("set", args)) {
			if (!sender.hasPermission(getCommandPermission() + ".set")) {
				throw new NoPermissionException(getCommandPermission() + ".set");
			}
			if (args.size() == 0) {
				throw new TooFewArgumentsException();
			}
			if (args.size() == 1) {
				if (!(sender instanceof Player)) {
					throw new TooFewArgumentsException();
				}
				target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
			}
			else {
				if (!sender.hasPermission(getCommandPermission() + ".set.others")) {
					throw new NoPermissionException(getCommandPermission() + ".set.others");
				}
				target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
			}
			String value = args.get(0);
			Integer xp = null;
			Integer levels = null;
			if (value.toLowerCase().startsWith("l")) {
				value = value.substring(1);
				try {
					levels = Integer.parseInt(value);
				}
				catch (NumberFormatException e) {
					throw new InvalidArgumentException("Cannot parse number: " + value);
				}
			}
			else {
				try {
					xp = Integer.parseInt(value);
				}
				catch (NumberFormatException e1) {
					throw new InvalidArgumentException("Cannot parse number: " + value);
				}
			}
			if (xp != null) {
				Experience.changeExp(target.getPlayer(), -Experience.getExp(target.getPlayer()));
				Experience.changeExp(target.getPlayer(), xp);
				//target.getPlayer().setTotalExperience(xp);
				target.getPlayer().playSound(target.getPlayer().getLocation(), Sound.ORB_PICKUP, 1.0f, 1.0f);
			}
			if (levels != null) {
				Experience.changeExp(target.getPlayer(), -Experience.getExpFromLevel((int) Experience.getLevelFromExp(Experience.getExp(target.getPlayer()))));
				Experience.changeExp(target.getPlayer(), Experience.getExpFromLevel(levels));
				//target.getPlayer().setLevel(levels);
				target.getPlayer().playSound(target.getPlayer().getLocation(), Sound.LEVEL_UP, 1.0f, 1.0f);
			}
			return new String[]{ChatColor.GOLD + target.getDisplayName() + ChatColor.AQUA + " now has " + ChatColor.GOLD
					+ Experience.getExp(target.getPlayer()) + ChatColor.AQUA + " exp (level " + ChatColor.GOLD
					+ (int) Experience.getLevelFromExp(Experience.getExp(target.getPlayer()))};
		}
		if (checkSubcommand("give", args)) {
			if (!sender.hasPermission(getCommandPermission() + ".give")) {
				throw new NoPermissionException(getCommandPermission() + ".give");
			}
			if (args.size() == 0) {
				throw new TooFewArgumentsException();
			}
			if (args.size() == 1) {
				if (!(sender instanceof Player)) {
					throw new TooFewArgumentsException();
				}
				target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
			}
			else {
				if (!sender.hasPermission(getCommandPermission() + ".set.give")) {
					throw new NoPermissionException(getCommandPermission() + ".set.give");
				}
				target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
			}
			String value = args.get(0);
			Integer xp = null;
			Integer levels = null;
			if (value.toLowerCase().startsWith("l")) {
				value = value.substring(1);
				try {
					levels = Integer.parseInt(value);
				}
				catch (NumberFormatException e) {
					throw new InvalidArgumentException("Cannot parse number: " + value);
				}
			}
			else {
				try {
					xp = Integer.parseInt(value);
				}
				catch (NumberFormatException e1) {
					throw new InvalidArgumentException("Cannot parse number: " + value);
				}
			}
			if (xp != null) {
				//target.getPlayer().giveExp(xp);
				Experience.changeExp(target.getPlayer(), xp);
				target.getPlayer().playSound(target.getPlayer().getLocation(), Sound.ORB_PICKUP, 1.0f, 1.0f);
			}
			if (levels != null) {
				//target.getPlayer().setLevel(target.getPlayer().getLevel() + levels);
				int desired_level = (int) (Experience.getLevelFromExp(Experience.getExp(target.getPlayer())) + levels);
				int desired_experience = Experience.getExpFromLevel(desired_level);
				desired_experience -= Experience.getExp(target.getPlayer());
				Experience.changeExp(target.getPlayer(), desired_experience);
				target.getPlayer().playSound(target.getPlayer().getLocation(), Sound.LEVEL_UP, 1.0f, 1.0f);
			}
			return new String[]{ChatColor.GOLD + target.getDisplayName() + ChatColor.AQUA + " now has " + ChatColor.GOLD
					+ Experience.getExp(target.getPlayer()) + ChatColor.AQUA + " exp (level " + ChatColor.GOLD
					+ (int) Experience.getLevelFromExp(Experience.getExp(target.getPlayer()))};
		}
		throw new InvalidArgumentException("Unknown subcommand. Try /help " + command);
	}
}
