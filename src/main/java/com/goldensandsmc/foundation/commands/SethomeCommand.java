package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.exception.TooManyHomesException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.LocationWrapper;
import com.goldensandsmc.core.util.Utils;
import com.goldensandsmc.foundation.player.data.Homes;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class SethomeCommand extends CoreCommand<Player> {

	public SethomeCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "sethome";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"createhome"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Creates a home"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> [player:][home]",
		};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.sethome";
	}

	@Override
	public String[] execute(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.size() == 0) {
			target = PlayerManager.getOrCreate(sender.getUniqueId()).getValue();
			int homes_count = target.getDataContainer(Homes.class).getHomesCount();
			if (!sender.hasPermission(getCommandPermission() + ".multiple") && !sender.hasPermission(getCommandPermission() + ".infinite")) {
				throw new TooManyHomesException(1);
			}
			Integer allowed_homes = Utils.getNumericPermission(sender, getCommandPermission() + ".multiple");
			if (allowed_homes != null && homes_count >= allowed_homes && !sender.hasPermission(getCommandPermission() + ".multiple")) {
				throw new TooManyHomesException(allowed_homes);
			}
			target.getDataContainer(Homes.class).addDefaultHome(new LocationWrapper(sender.getLocation()));
		}
		else {
			String[] parts = args.get(0).split(":", 2);
			if (parts.length > 1) {
				if (!sender.hasPermission(getCommandPermission() + ".others")) {
					throw new NoPermissionException(getCommandPermission() + ".others");
				}
				target = PlayerManager.getOrCreateByAnyName(parts[0]).getValue();
			}
			else {
				target = PlayerManager.getOrCreate(sender.getUniqueId()).getValue();
			}
			String home_name = parts[parts.length - 1];
			target.getDataContainer(Homes.class).addHome(home_name, new LocationWrapper(sender.getLocation()));
		}
		return new String[]{"Home set!"};
	}
}
