package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.player.data.Nickname;
import com.goldensandsmc.core.player.data.PlayerGeoIP;
import com.goldensandsmc.core.util.GeoIP;
import com.goldensandsmc.core.util.Utils;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.LinkedList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/28/2017.
 */
public class WhoisCommand extends CoreCommand<CommandSender> {

	public WhoisCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "whois";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"who"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Shows a player's information"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <player>"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.whois";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.isEmpty()) {
			throw new TooFewArgumentsException();
		}
		PlayerContainer target = PlayerManager.getOrCreateByAnyNamePartial(args.get(0)).getValue();
		LinkedList<String> lines = new LinkedList<>();
		lines.add(ChatColor.AQUA + "Name: " + ChatColor.GOLD + target.getName());
		Nickname nickname = target.getDataContainer(Nickname.class);
		if (nickname.getNickname() != null) {
			lines.add(ChatColor.AQUA + "Nickname: " + ChatColor.GOLD + nickname.getNickname());
		}
		lines.add(ChatColor.AQUA + "UUID: " + ChatColor.GOLD + Utils.stringFromUuid(target.getUuid()));
		lines.add(ChatColor.AQUA + "Online: " + Utils.formatYesNo(target.isOnline()));
		lines.add(ChatColor.AQUA + "First joined: " + ChatColor.GOLD + DateFormat.getDateInstance(DateFormat.MEDIUM).format(target.getFirstJoined()));
		lines.add(ChatColor.AQUA + "Last seen: " + ChatColor.GOLD + DateFormat.getDateInstance(DateFormat.MEDIUM).format(target.getLastSeen()));
		if (sender.hasPermission(getCommandPermission() + ".ip")) {
			lines.add(ChatColor.AQUA + "IP: " + ChatColor.GOLD + target.getAddress().toString());
		}
		if (sender.hasPermission(getCommandPermission() + ".geoip")) {
			GeoIP geoip = target.getDataContainer(PlayerGeoIP.class).getGeoIP();
			if (geoip == null || !geoip.exists()) {
				lines.add(ChatColor.AQUA + "GeoIP Location: " + ChatColor.RED + "unknown");
			}
			else {
				lines.add(ChatColor.AQUA + "Location: " + ChatColor.GOLD + geoip.getCity() + ", " + geoip.getRegionName() + ", " + geoip.getCountryName());
				lines.add(ChatColor.AQUA + "Timezone: " + ChatColor.GOLD + geoip.getTimeZone());
			}
		}
		lines.add(ChatColor.AQUA + "Banned: " + Utils.formatYesNo(target.getOfflinePlayer().isBanned()));
		if (getPlugin().getServer().hasWhitelist()) {
			lines.add(ChatColor.AQUA + "Whitelisted: " + Utils.formatYesNo(target.getOfflinePlayer().isWhitelisted()));
		}
		lines.add(ChatColor.AQUA + "OP: " + Utils.formatYesNo(target.getOfflinePlayer().isOp()));
		return lines.toArray(new String[0]);
	}
}
