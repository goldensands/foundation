package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CooldownCommand;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.foundation.SandcastleFoundation;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/14/2017.
 */
public class HealCommand extends CooldownCommand<CommandSender> {

	public HealCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public int getCooldownSeconds() {
		return SandcastleFoundation.getInstance().getJsonConfig().getNode("commands.heal.cooldown_seconds", int.class, 0);
	}

	@Override
	public String getCommandName() {
		return "heal";
	}

	@Override
	public String[] getCommandAliases() {
		return null;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Heals a player"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player]"};
	}

	@Override
	public String getCommandPermission() {
		return "sancastle.foundation.command.heal";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.isEmpty()) {
			if (!(sender instanceof Player)) {
				throw new TooFewArgumentsException();
			}
			target = PlayerManager.getOrCreate((OfflinePlayer) sender).getValue();
		}
		else {
			if (!sender.hasPermission(getCommandPermission() + ".others")) {
				throw new NoPermissionException(getCommandPermission() + ".others");
			}
			target = PlayerManager.getOrCreateByAnyName(args.remove(0)).getValue();
		}
		EntityRegainHealthEvent event = new EntityRegainHealthEvent(target.getPlayer(), target.getPlayer().getMaxHealth(), EntityRegainHealthEvent.RegainReason.CUSTOM);
		getPlugin().getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return new String[]{"Failed to heal "
					+ (sender.equals(target.getPlayer()) ? "yourself" : ChatColor.GOLD + target.getDisplayName())};
		}
		target.getPlayer().setHealth(target.getPlayer().getMaxHealth());
		return new String[]{
				(sender.equals(target.getPlayer()) ? "You" : ChatColor.GOLD + target.getDisplayName())
						+ ChatColor.AQUA + " have been healed"
		};
	}
}
