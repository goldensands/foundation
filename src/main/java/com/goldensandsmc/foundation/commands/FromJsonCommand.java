package com.goldensandsmc.foundation.commands;

import com.goldensandsmc.core.commands.CoreCommand;
import com.goldensandsmc.core.exception.NoSuchPlayerException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.JsonUtils;
import com.google.common.base.Joiner;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import java.sql.SQLException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public class FromJsonCommand extends CoreCommand<CommandSender> {

	public FromJsonCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "fromjson";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"itemfromjson", "jsontoitem", "toitem"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Converts a valid JSON string into an item"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player] <json>"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.foundation.command.fromjson";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.isEmpty()) {
			throw new TooFewArgumentsException();
		}
		PlayerContainer target;
		try {
			target = PlayerManager.getOrCreateByAnyName(args.get(0)).getValue();
			args.remove(0);
		}
		catch (NoSuchPlayerException e) {
			if (!(sender instanceof Player)) {
				throw e;
			}
			target = PlayerManager.getOrCreate(((Player) sender).getUniqueId()).getValue();
		}
		String raw_json = Joiner.on(' ').join(args);
		try {
			ItemStack item = JsonUtils.fromJson(raw_json, ItemStack.class);
			for (ItemStack overflow : target.getPlayer().getInventory().addItem(item).values()) {
				target.getPlayer().getWorld().dropItem(target.getPlayer().getLocation().add(0, 0.25, 0), overflow);
			}
			return new String[]{"Successfully converted item"};
		}
		catch (Exception e) {
			throw new CommandException(e);
		}
	}
}
