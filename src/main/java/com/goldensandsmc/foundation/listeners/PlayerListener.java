package com.goldensandsmc.foundation.listeners;

import com.goldensandsmc.core.exception.NoSuchPlayerException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.util.LocationWrapper;
import com.goldensandsmc.foundation.player.data.Homes;
import com.goldensandsmc.foundation.util.Mailbox;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/23/2017.
 */
public class PlayerListener implements Listener {
	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		try {
			PlayerContainer container = PlayerManager.getOrCreate(event.getPlayer()).getValue();
			// Get unread mail
			int unread_mail = Mailbox.getMessageCount(event.getPlayer().getUniqueId(), false);
			if (unread_mail > 0) {
				event.getPlayer().sendMessage(new String[]{
						ChatColor.AQUA + "You have " + ChatColor.GOLD + unread_mail + ChatColor.AQUA + " new message(s)",
						ChatColor.AQUA + "Use " + ChatColor.GOLD + "/mail read [page]" + ChatColor.AQUA + " to view your messages"
				});
			}
		}
		catch (NoSuchPlayerException | SQLException e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void updateBedHome(final PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		if (event.getClickedBlock().getType().name().equalsIgnoreCase("bed")) {
			try {
				PlayerContainer container = PlayerManager.getOrCreate(event.getPlayer().getUniqueId()).getValue();
				container.getDataContainer(Homes.class).addHome("bed", new LocationWrapper(event.getClickedBlock().getLocation().add(0.5, 0.0, 0.5)));
			}
			catch (NoSuchPlayerException | SQLException e) {
				e.printStackTrace();
			}
			event.getPlayer().setBedSpawnLocation(event.getClickedBlock().getLocation());
			event.getPlayer().sendMessage(ChatColor.AQUA + "Bed spawn set!");
		}
	}
}
